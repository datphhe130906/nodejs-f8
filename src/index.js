const express = require('express');
const morgan = require('morgan');
const path = require('path');
var bodyParser = require('body-parser')
const handlebars = require('express-handlebars');
const app = express();
const port = 3000;
const renameRouter = require('./resources/routes/rename.route')
const shareRouter = require('./resources/routes/share.route');
//template engine;
app.engine('hbs', handlebars({
    extname: '.hbs'
}));

// Edit extension .handlebars = hbs (short);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources','views'));


///Middleware
//Http Logger
          // app.use(morgan('combined'))
// Join folder Public
app.use(express.static(path.join(__dirname, 'public')));

// Body parser
app.use(bodyParser.urlencoded({extended: false}));
// Share Accoutn
app.use(shareRouter);
// Rename Ads
app.use(renameRouter)

// HOME 
app.get('/', function (req, res) {
  return res.render('home')
  })
app.listen(port, () => console.log(`Port ${port}`));// Khoi tao sever port 300