const express = require('express');
const router = express.Router();
const rename = require('../../controllers/rename.controller');
router.get('/rename', function (req, res) {
    return res.render('rename')
  })
router.post('/rename', rename)
module.exports = router;