const express = require('express');
const router = express.Router();
const share = require('../../controllers/share.controller');
router.get('/share', function (req, res) {  
    return res.render('share')
    })
router.post('/share', share)

module.exports = router;